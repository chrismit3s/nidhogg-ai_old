from src.autoencoder.build import get_model_from_desc
from keras.models import Model

if __name__ == "__main__":
    desc = ["conv 18   # comment",
            "scale 2x3",
            "dense 5"] 
    orig, enc, dec = get_model_from_desc(desc)
    model = Model(orig, dec)
    model.summary()