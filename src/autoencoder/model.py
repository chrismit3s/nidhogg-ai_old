from keras.models import load_model, Model
from keras.callbacks import TensorBoard
from time import strftime, time
from os import listdir
from src.autoencoder.build import get_autoencoder_from_desc, split_autoencoder
from src.files import autoencoder_models, models_overview, arrays, logs
from src.autoencoder.dataset import data_format, load_data


optimizer = "adadelta"
loss = "binary_crossentropy"
epochs = 30
batch_size = 8


class AutoEncoder():
    def __init__(self, model_data=None):
        # set the model
        if isinstance(model_data, int):  # load trained model
            self.load_layers(model_data)
        elif isinstance(model_data, list):  # create new model from list
            self.create_layers(model_data)
        elif model_data is None:  # no model specified
            self.desc = None
            self.layers = None

    def create_layers(self, desc):
        self.desc = desc
        self.autoencoder = get_autoencoder_from_desc(desc)
        self.encoder, self.decoder = split_autoencoder(self.autoencoder)

    def load_layers(self, n):
        # wrap indexes around
        if n < 0:
            n += autoencoder_models.num_files()

        self.desc = None
        self.autoencoder = load_model(autoencoder_models.full_name().format(n))
        self.encoder, self.decoder = split_autoencoder(self.autoencoder)

    def save(self):
        if self.layers is not None:
            print("Saving model", end="", flush=True)
            filename = autoencoder_models.full_name().format(autoencoder_models.num_files())
            with open(models_overview, mode="a+") as overview:
                # write header
                overview.write(f"{filename} {strftime('%Y-%m-%dT%H-%M-%S')}")
                # write description/architecture
                overview.write("\n    ".join([""] + self.desc))
                overview.write("\n\n")
            self.autoencoder.save(filename)
            print(" - done")
        else:
            raise ValueError("No model set")

    def compile(self):
        if self.autoencoder is not None:
            self.autoencoder.compile(loss=loss, optimizer=optimizer)
            self.encoder.compile(loss=loss, optimizer=optimizer)
            self.decoder.compile(loss=loss, optimizer=optimizer)
        else:
            raise ValueError("No model set")

    def train(self, verbose=True):
        # load last data array as testing data
        if verbose:
            print("Loading testing data", end="", flush=True)
        t = time()
        x_test = load_data(-1).astype("float32") / 255
        if verbose:
            print(" - done - took {time() - t:.0f}s")

        # iterate over all data arrays and train
        try:
            t = time()
            for i in range(arrays.num_files() - 1):
                if verbose:
                    print(f"Loading training data #{i}", end="", flush=True)
                    t = time()
                x_train = load_data(i).astype("float32") / 255
                if verbose:
                    print(f" - done - took {time() - t:.0f}s")
                print("TRAINING RIGHT NOW")
                #self.autoencoder.fit(x_train, x_train,
                #                     epochs=epochs,
                #                     batch_size=batch_size,
                #                     shuffle=True,
                #                     verbose=1 if verbose else 0,
                #                     validation_data=(x_test, x_test),
                #                     callbacks=[TensorBoard(log_dir=logs)])
                del x_train
        except KeyboardInterrupt:
            if verbose:
                print(f"Training model - stopped after {time() - t:.0f}s")
        else:
            if verbose:
                print(f"Training model - done - took {time() - t:.0f}s")


