from keras.models import Model
#from src.autoencoder.build import build_model
from src.autoencoder.dataset import load_data
from src.files import autoencoder_models
from time import localtime, strftime, time
import keras


model_path = r"models\autoencoder-{time}.h5"
model_type = 0
optimizer = "adadelta"
loss = "binary_crossentropy"
epochs = 30
batch_size = 8


def train(model, batches, verbose, **kwargs):
    for i in batches:
        if verbose:
            print(f"Loading training data #{i}", end="\r", flush=True)
            t = time()
        x_train = load_data(i).astype("float32") / 255
        if verbose:
            print(f"Loading training data #{i} - done - took {time() - t:.0f}s")
        autoencoder.fit(x_train, x_train, **kwargs)
        del x_train


if __name__ == "__main__":
    # load last batch as testing data
    print("Loading testing data", end="\r", flush=True)
    t = time()
    x_test = load_data(-1).astype("float32") / 255
    print("Loading testing data - done - took {time() - t:.0f}s")
    # get shape and format
    shape = x_test.shape[1:]
    data_format = "channels_last" if shape[-1] <= 4 else "channels_first"

    # log settings
    print("Settings are")
    print(f"  model_path  = {model_path}")
    print(f"  model_type  = {model_type}")
    print(f"  data_shape  = {shape}")
    print(f"  data_format = {data_format}")
    print(f"  optimizer   = {optimizer}")
    print(f"  loss        = {loss}")
    print(f"  epochs      = {epochs}")
    print(f"  batch_size  = {batch_size}")

    # build model
    print("Building model", end="\r", flush=True)
    t = time()
    (original, encoded, decoded) = build_model(model_type, shape, data_format)
    autoencoder = Model(original, decoded)
    print("Building model - done - took {time() - t:.0f}s, summary:")
    autoencoder.summary(print_fn=lambda *args, **kwargs: print(" ", *args, **kwargs) if args[0].replace("_", "") != "" else None)
    autoencoder.compile(optimizer=optimizer, loss=loss)

    # train model
    try:
        t = time()
        train(autoencoder,
              range(num_batches - 1),
              True,
              shuffle=True,
              verbose=1,
              epochs=epochs,
              batch_size=batch_size,
              validation_data=(x_test, x_test))
    except KeyboardInterrupt:
        print(f"Training model - stopped after {time() - t:.0f}s")
    else:
        print(f"Training model - done - took {time() - t:.0f}s")


    # save model
    print("Saving model", end="\r", flush=True)
    autoencoder.save(model_path.format(time=strftime('%Y-%m-%dT%H-%M-%S', localtime())))
    print("Saving model - done")
