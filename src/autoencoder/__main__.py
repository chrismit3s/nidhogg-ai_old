from src.autoencoder.model import AutoEncoder
from src.files import autoencoder_models
from keras.models import Model, load_model
from keras.layers import Input
from src.autoencoder.build import get_autoencoder_from_desc, split_autoencoder
from src.autoencoder.dataset import load_data
import numpy as np
import cv2


desc = ["conv 18",
        "scale 2x3",
        "dense 18000"]

autoencoder = AutoEncoder(0)
autoencoder.compile()

print_fn = lambda *args, **kwargs: (print(" ", *args, **kwargs)
                                    if args[0].replace("_", "") != ""
                                    else None)
print("AUTOENCODER")
autoencoder.autoencoder.summary(print_fn=print_fn)
print("ENCODER")
autoencoder.encoder.summary(print_fn=print_fn)
print("DECODER")
autoencoder.decoder.summary(print_fn=print_fn)

x_train = np.array(list(load_data(0)) + list(load_data(1)))
for frame in x_train:
    cv2.imshow("Original frame", frame)
    cv2.imshow("Autoencoded frame",
               cv2.cvtColor(autoencoder.autoencoder.predict(cv2.cvtColor(frame,
                                                             cv2.COLOR_RGB2RGBA)[np.newaxis, ...]).squeeze(),
                            cv2.COLOR_RGBA2RGB))  # RGB to RGBA is the same as BGR TO BGRA

    encoded = autoencoder.encoder.predict(cv2.cvtColor(frame, cv2.COLOR_RGB2RGBA)[np.newaxis, ...])
    cv2.imshow("Encoded frame", cv2.resize(encoded, (0, 0), fx=30, fy=100))

    key = cv2.waitKey(0)
    if key == 27 or key == ord("q"):
        cv2.destroyAllWindows()
        break
