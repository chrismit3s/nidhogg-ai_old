from numpy import asarray
from os import listdir
from os.path import join
from src import FRAME_SIZE
from src.files import arrays
from src.helpers import vidread, vidshow
from src.nidhogg.gamehandle import GameHandle
from time import time
import cv2
import numpy as np


def extract_frames(filename, step=8):
    for frameno, frame in enumerate(vidread(filename)):
        if frameno % step == 0:
            yield frame
    pass


def prepare_frames(frame_gen, interpolation="lanczos4"):
    def _frame_gen():
        for frame in frame_gen:
            yield cv2.resize(frame,
                             dsize=FRAME_SIZE,
                             interpolation=interpolation)
        pass

    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    gh = GameHandle((0, 0, 0, 0))
    for p in range(2):
        for frame in gh.prep_frame(player=p, frame_gen=_frame_gen()):
            yield frame
    pass
        

def create_arrays(frame_gen, blocks=16, verbose=True):
    data = [[] for i in range(blocks)]

    # load the frames
    i = 0
    t = time()
    if verbose:
        print(f"Loading frames", end="", flush=True)
    for frame in frame_gen:
        data[i % blocks].append(frame)
        i += 1
    if verbose:
        print(f" - done - took {time() - t:.0f}s")


    # Save them
    t = time()
    for i in range(blocks):
        if verbose:
            print(f"Saving block #{i + 1} - {100 * (i + 1) // blocks:>3}%", end="\r", flush=True)
        np.save(arrays.full_name(i), np.asarray(data[i]))
    if verbose:
        print(f"Saving blocks - done - took {time() - t:.0f}s")
    pass


def prepare_data(filename,
                 step=8,
                 interpolation="lanczos4",
                 blocks=16,
                 verbose=True):
    create_arrays(prepare_frames(extract_frames(filename,
                                                step),
                                 interpolation),
                  blocks,
                  verbose)
    pass
   


def load_data(block=0):
    # wrap indexes around (pythonic ;) )
    if block < 0:
        block += arrays.num_files()

    # load data
    return np.load(arrays.full_name(block))
