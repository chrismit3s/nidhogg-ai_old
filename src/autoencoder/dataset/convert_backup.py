from os import listdir
from os.path import join
from src import FRAME_SIZE
from src.files import extracted_frames, prepared_frames, arrays
from src.helpers import vidread, vidshow
from src.nidhogg.gamehandle import GameHandle
from time import time
from numpy import asarray
import cv2
import numpy as np


def extract_frames(dir, step=8, verbose=True):
    t = time()
    for fileno, filename in enumerate(listdir(dir)):
        if verbose:
            print(f"Extracting frames from {filename}", end="\r", flush=True)
        for frameno, frame in enumerate(vidread(join(dir, filename))):
            if frameno % step == 0:
                cv2.imwrite(extracted_frames.full_name(frameno // step),
                            frame)
    if verbose:
        print(f"Extracting frames - done - took {time() - t:.0f}s" + 40 * " ")


def prepare_frames(dir, interpolation="lanczos4", verbose=True):
    def frame_gen():
        for filename in listdir(dir):
            yield cv2.resize(cv2.imread(join(dir, filename)),
                             dsize=FRAME_SIZE,
                             interpolation=interpolation)
        pass

    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    i = 0
    t = time()
    num_frames = len(listdir(dir))
    gh = GameHandle((0, 0, 0, 0))
    for p in range(2):
        for frame in gh.prep_frame(player=p, frame_gen=frame_gen()):
            if verbose:
                print(f"Preparing frames - {(100 * i) // (2 * num_frames):>3}%", end="\r", flush=True)
            cv2.imwrite(prepared_frames.full_name(i), frame)
            i += 1
    if verbose:
        print(f"Preparing frames - done - took {time() - t:.0f}s")
        

def create_arrays(dir, blocks=16, verbose=True):
    data = [[] for i in range(blocks)]

    # load the frames
    i = 0
    t = time()
    num_frames = len(listdir(dir))
    for filename in listdir(dir):
        frame = cv2.imread(join(dir, filename))
        if verbose:
            print(f"Loading frames - {(100 * i) // (2 * num_frames):>3}%", end="\r", flush=True)
        data[i % blocks].append(frame)
        i += 1
    if verbose:
        print(f"Loading frames - done - took {time() - t:.0f}s")


    # Save them
    t = time()
    for i in range(blocks):
        if verbose:
            print(f"Saving block #{i + 1} - {100 * (i + 1) // blocks:>3}%", end="\r", flush=True)
        np.save(arrays.full_name(i), np.asarray(data[i]))
    if verbose:
        print(f"Saving blocks - done - took {time() - t:.0f}s")


def load_data(block=0):
    # wrap indexes around (pythonic ;) )
    if block < 0:
        block += arrays.num_files()

    # load data
    return np.load(arrays.full_name(block))
