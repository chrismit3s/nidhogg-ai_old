from argparse import ArgumentParser
from src.autoencoder.dataset import prepare_data
from src.files import gameplay_video
from src.helpers import vidshow

parser = ArgumentParser()
parser.add_argument("-v", "--verbose",
                    action="store_true",
                    help="more verbose output")
parser.add_argument("--step",
                    type=int,
                    default=8,
                    help="the frame steps when extracting the frames")
parser.add_argument("--interpolation",
                    type=str,
                    default="lanczos4",
                    help="the interpolation when preparing the frames")
parser.add_argument("--blocks",
                    type=int,
                    default=16,
                    help="the number of blocks to split the data into")
args = parser.parse_args()


prepare_data(gameplay_video,
             args.step,
             args.interpolation,
             args.blocks,
             args.verbose)
