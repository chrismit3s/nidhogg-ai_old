from src.autoencoder.dataset.convert import (
    extract_frames,
    prepare_frames,
    create_arrays,
    prepare_data,
    load_data)

data_format = "channels_last"

