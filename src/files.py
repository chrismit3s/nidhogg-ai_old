from collections import namedtuple
from os.path import join
import os


class Filename(namedtuple("_Filename", ["path", "name"])):
    def full_name(self, *args, **kwargs):
        return join(self.path, self.name if (args == () and kwargs == {}) else self.name.format(*args, **kwargs))

    def num_files(self):
        return len(os.listdir(self.path))

    def iter_names(self):
        for i in range(self.num_files()):
            yield self.full_name().format(i)


autoencoder_models = Filename(join("models", "autoencoder"), "autoencoder-{:02}.h5")
arrays             = Filename(join("data",   "arrays"),      "array-{:02}.npy")
gameplay_video     = join("data", "gameplay.mp4")
appdata            = join(f"C:{os.sep}", "Users", "Chrissidtmeier", "AppData", "Roaming", "Nidhogg")
models_overview    = join("models", "overview.txt")
logs               = join("logs")


