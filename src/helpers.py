from pynput.keyboard import Key, KeyCode
from multiprocessing import Process
from numpy import asarray
import numpy as np
import cv2
import inspect
import logging as log


def vidread(filename):
    cap = cv2.VideoCapture(filename)

    # make sure stream is open
    if not cap.isOpened():
        cap.open()

    # ret turns false when stream is done
    ret = True
    while ret:
        ret, frame = cap.read()
        yield frame

    # close stream
    cap.release()


def vidshow(frame_gen, zoom=1, interpolation="nearest", title=None, delay=1):
    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    # if zoom is an int, make it a tuple
    if isinstance(zoom, int):
        zoom = (zoom, zoom)

    # use generator representation as default title
    title = title or f"{frame_gen!r}"
    for frame in frame_gen:
        # update the window
        cv2.imshow(title,
                   cv2.resize(frame,
                              dsize=(0, 0),
                              fx=zoom[0],
                              fy=zoom[1],
                              interpolation=interpolation))

        # check for close
        key = cv2.waitKey(delay)
        if key == 27 or key == ord("q") or cv2.getWindowProperty(title, 0) == -1:  # esc or q or window-x
            cv2.destroyWindow(title)
            break


def get_key(key):
    if key.lower() in dir(Key):
        return Key[key.lower()]
    else:
        return KeyCode(char=key)


def replace_color(img, target, margin, result):
    img[(target - margin <= img).all(axis=2) &
        (target + margin >= img).all(axis=2)] = result


def win_reward(t, s): 
    return (30 * s) / (t + 60) + 1 / 2


def reward_table(reward_func):
    times = list(range(0, 151, 30))
    r = []

    print("t\s |     win    lose")
    print("----+----------------")
    for time in times:
        r.append((reward_func(time, 1), reward_func(time, -1)))
        print(f"{time:3} | {r[-1][0]: 7.2f} {r[-1][1]: 7.2f}")

    if r[0][0] > r[1][0] and \
       r[1][0] > r[1][1] and \
       r[1][1] > r[0][1]:
        s = inspect.getsource(reward_func)
        s = s[s.find("return") + 7:-1]
        print("")
        print(f"r(t, s) = {s}  is a possible candidate")
