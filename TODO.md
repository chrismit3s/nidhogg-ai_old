# todo

## nidhogg

* subprocess thingy as decorator
  * no `get_keyboard` method, remove everything
    unpicklable before pickle and create
    everything again after `fork`/`spawn`
* **listener process to start events**
* split gamehandle into:
  * a handle object for soley game IO related things
  * a "user interface" (render, focus_and_unpause)

## autoencoder 

* train on tensorpad
* PCA on results

### dataset

* make preperation functions generators, and dont store every thing on disk, this repo is huge

## q-learning

* [resource][q-learning blog]
* action: one key of controls
* state: next frame
* rewards (reward for opponent -> punish): 
  * for win (win_reward in helpers)
  * for gaining(2x)/holding(1x) go
  * for pressing left (if on go)
  * for a transition in the right direction

## readme

* controls file format reverse engineering
* some notes for readme regarding the model build:
  * [training data][training data]
  * output must be the same as input
  * input is 389x243x3 downsampling/pooling makes rounding errors
  * upsampling results in different size because of those errors
  * cropping!!

[training data]: https://www.youtube.com/playlist?list=PLMrjV7ZpwlD3WNiU-2dZvb452LT_0bWBG
[q-learning blog]: https://adventuresinmachinelearning.com/reinforcement-learning-tutorial-python-keras/