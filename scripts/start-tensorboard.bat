@echo off

REM path + venv setup
d:
cd d:\chrissidtmeier\projects\nidhogg-ai\
call "d:\chrissidtmeier\projects\nidhogg-ai\venv\scripts\activate.bat" >nul 2>&1

start "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" http://127.0.0.1:1337/
tensorboard --logdir ./logs --host 127.0.0.1 --port 1337